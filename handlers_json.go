package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func getRoutes(c *gin.Context) {
	routes := `
	json := router.Group("/api/json")
	json.GET("/cinemas", getCinemas)
	json.GET("/playing/", getPlaying)
	json.GET("/upcoming/", getUpcoming)
	json.GET("/showing/", getShowing)
	json.GET("/admission", admission)

	xml := router.Group("/api/xml")
	xml.GET("/cinemas", getCinemasXML)
	xml.GET("/playing/", getPlayingXML)
	xml.GET("/upcoming/", getUpcomingXML)
	xml.GET("/showing/", getShowingXML)
	xml.GET("/admission", admissionXML)
	`
	c.Writer.Write([]byte(routes))
}

func getCinemas(c *gin.Context) {
	cinemas, err := palaceApi.GetCinemas()
	if err != nil {
		c.Abort()
	}

	c.IndentedJSON(http.StatusOK, cinemas)
	c.Abort()
}

func getPlaying(c *gin.Context) {
	url := c.Query("link")
	playing, err := palaceApi.GetPlaying(url)
	if err != nil {
		c.Abort()
	}

	c.IndentedJSON(http.StatusOK, playing)
	c.Abort()
}

func getUpcoming(c *gin.Context) {
	comingSoon, err := palaceApi.GetComingSoon()
	if err != nil {
		c.Abort()
	}

	c.IndentedJSON(http.StatusOK, comingSoon)
	c.Abort()
}

func getShowing(c *gin.Context) {
	nowShowing, err := palaceApi.GetNowShowing()
	if err != nil {
		c.Abort()
	}

	c.IndentedJSON(http.StatusOK, nowShowing)
	c.Abort()
}

func admission(c *gin.Context) {
	admission, err := palaceApi.GetAdmissions()
	if err != nil {
		c.Abort()
	}

	c.IndentedJSON(http.StatusOK, admission)
	c.Abort()
}
