package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	api "gitlab.com/kingwill101/palaceapi/api"
)

type Cinemas struct {
	Version string       `xml:"version,attr"`
	Cinemas []api.Cinema `xml:"cinemas"`
}

type Playing struct {
	Version string      `xml:"version,attr"`
	Playing []api.Movie `xml:"movie"`
}

type UpComing struct {
	Version string      `xml:"version,attr"`
	Playing []api.Movie `xml:"movie"`
}
type Showing struct {
	Version string      `xml:"version,attr"`
	Showing []api.Movie `xml:"showing"`
}
type Admissions struct {
	Version    string `xml:"version,attr"`
	Collection api.AdmissionCollection
}

func getCinemasXML(c *gin.Context) {
	cinemas, err := palaceApi.GetCinemas()
	if err != nil {
		c.Abort()
	}

	c.XML(http.StatusOK, &Cinemas{
		Version: "1",
		Cinemas: cinemas,
	})

	c.Abort()
}

func getPlayingXML(c *gin.Context) {
	url := c.Query("link")
	playing, err := palaceApi.GetPlaying(url)
	if err != nil {
		c.Abort()
	}

	c.XML(http.StatusOK, &Playing{
		Version: "1",
		Playing: playing,
	})
	c.Abort()
}

func getUpcomingXML(c *gin.Context) {
	comingSoon, err := palaceApi.GetComingSoon()
	if err != nil {
		c.Abort()
	}

	c.XML(http.StatusOK, &UpComing{
		Version: "1",
		Playing: comingSoon,
	})
	c.Abort()
}

func getShowingXML(c *gin.Context) {
	nowShowing, err := palaceApi.GetNowShowing()
	if err != nil {
		c.Abort()
	}

	showing := Showing{
		Version: "1",
		Showing: nowShowing,
	}

	c.XML(http.StatusOK, showing)
	c.Abort()
}

func admissionXML(c *gin.Context) {
	admission, err := palaceApi.GetAdmissions()
	if err != nil {
		c.Abort()
	}

	adm := Admissions{
		Version:    "1",
		Collection: admission,
	}

	c.XML(http.StatusOK, adm)
	c.Abort()
}
