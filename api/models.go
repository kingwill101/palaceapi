package palaceapi

type Movie struct {
	Title       string `xml:"title"`
	Time        string `xml:"time"`
	Link        string `xml:"link"`
	Image       string `xml:"image"`
	Description string `xml:"description"`
	Ratings     string `xml:"ratings"`
	Genre       string `xml:"genre"`
	RunningTime string `xml:"running_time"`
	ReleaseDate string `xml:"release_date"`
	Cast        string `xml:"cast"`
	Trailer     string `xml:"trailer"`
}

type Cinema struct {
	Name string `xml:"name"`
	Code string `xml:"code"`
	Link string `xml:"link"`
}

type ComingSoon struct {
	Name    string `xml:"name"`
	Image   string `xml:"image"`
	Trailer string `xml:"trailer"`
}

type AdmissionPricing struct {
	Regular string `xml:"regular"`
	Box     string `xml:"box"`
	SkyBox  string `xml:"skybox"`
}

type AdmissionItem struct {
	Name  string           `xml:"name"`
	Price AdmissionPricing `xml:"tiers"`
}

type Admission struct {
	Cinema string          `xml:"cinema"`
	Prices []AdmissionItem `xml:"admission_item"`
}

type AdmissionCollection struct {
	Items []Admission `xml:"cinema_admission"`
}
