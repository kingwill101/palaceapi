package palaceapi

import (
	"strings"
	"unicode/utf8"

	"github.com/PuerkitoBio/goquery"
	//"fmt"
)

type Api struct {
	baseUrl string
}

func (api *Api) GetCinemas() (cinemas []Cinema, err error) {
	var doc *goquery.Document

	doc, err = goquery.NewDocument("https://www.palaceamusement.com/allcinemas.php")

	if err != nil {
		return
	}

	// Find the review items
	_ = doc.Find("div .inner-wrap").First().Children().Each(func(i int, s *goquery.Selection) {
		link, found := s.Find("a").First().Attr("href")

		if !found {
			link = ""
		}

		index := strings.Index(link, "=")
		code := link[index+1:]

		cinema := Cinema{
			Name: s.Text(),
			Link: api.baseUrl + link,
			Code: code,
		}
		cinemas = append(cinemas, cinema)

	})
	return
}

func (api *Api) GetPlaying(url string) (playing []Movie, err error) {

	var doc *goquery.Document

	doc, err = goquery.NewDocument(url)

	if err != nil {
		return
	}

	_ = doc.Find("div .inner-wrap").First().Children().Each(func(i int, s *goquery.Selection) {

		titleSearch := s.Find(".description .movie-header .col-md-9 h2")
		link, _ := titleSearch.Find("a").Attr("href")
		name := titleSearch.Text()
		plot := s.Find(".description .plot  .col-md-12 p").Text()
		imageClass := s.Find(".image")
		time := s.Find(".description .showtimes-reviews .showtimes p").Text()
		image, _ := imageClass.Find("a img").Attr("src")
		rating := s.Find(".description .movie-header .col-md-9 .badge .rating").Text()

		m := Movie{
			Title:       name,
			Description: plot,
			Image:       image,
			Link:        link,
			Time:        time,
			Ratings:     rating,
		}
		_ = s.Find(".description ul").Children().Each(func(i int, child *goquery.Selection) {
			line := child.Text()
			if strings.HasPrefix(line, "Genre:") {
				genre := line[utf8.RuneCountInString("Genre:"):]
				m.Genre = genre
			}

			if strings.HasPrefix(line, "Running Time:") {
				rTime := line[utf8.RuneCountInString("Running Time:"):]
				m.RunningTime = rTime
			}

			if strings.HasPrefix(line, "Release Date:") {
				rDate := line[utf8.RuneCountInString("Release Date:"):]
				m.ReleaseDate = rDate
			}

			if strings.HasPrefix(line, "Cast:") {
				cast := line[utf8.RuneCountInString("Cast:"):]
				htmlClean := strings.Replace(cast, "\n", "", -1)
				excessSpaceClean := strings.Replace(htmlClean, "   ", "", -1)

				m.Cast = excessSpaceClean
			}

		})

		playing = append(playing, m)
	})
	return
}

func (a *Api) SetBaseUrl(url string) {
	a.baseUrl = url
}

func (a *Api) GetComingSoon() (coming []Movie, err error) {
	var doc *goquery.Document

	doc, err = goquery.NewDocument(a.baseUrl)

	if err != nil {
		return
	}

	_ = doc.Find(".cs-cycle-wrap .cycle-slideshow").First().Children().Each(func(i int, s *goquery.Selection) {
		title, _ := s.Find("a").First().Attr("title")
		trailer, _ := s.Find("a").First().Attr("href")
		img, _ := s.Find("a img").First().Attr("src")

		soon := Movie{
			Title:   title,
			Image:   img,
			Trailer: trailer,
		}
		coming = append(coming, soon)
	})
	return

}

func (a *Api) GetNowShowing() (nowShowing []Movie, err error) {
	var doc *goquery.Document

	doc, err = goquery.NewDocument(a.baseUrl)

	if err != nil {
		return
	}

	_ = doc.Find(".ns-cycle-wrap .cycle-slideshow").First().Children().Each(func(i int, s *goquery.Selection) {
		title := s.Find("a").First().Children().Remove().Text()
		trailer, _ := s.Find("a").First().Attr("href")
		img, _ := s.Find("a img").First().Attr("src")

		showing := Movie{
			Title:   strings.Replace(title, "Watch Trailer", "", -1),
			Image:   img,
			Trailer: trailer,
		}
		nowShowing = append(nowShowing, showing)
	})
	return

}

//GetAdmissions Return admission pricing for all
func (a *Api) GetAdmissions() (collection AdmissionCollection, err error) {

	var doc *goquery.Document
	//url := "http://localhost:8080/pricing/"
	url := "https://www.palaceamusement.com/admission.php"
	doc, err = goquery.NewDocument(url)

	if err != nil {
		return
	}

	_ = doc.Find(".table-wrap .table-responsive").Each(func(i int, s *goquery.Selection) {
		var adm Admission
		adm.Cinema = s.Find(".theatre-name").Text()

		//each row in table
		s.Find(".table tbody").Children().Each(func(j int, tr *goquery.Selection) {
			var item AdmissionItem

			//td 6 is junk so ignore it
			if j != 6 {
				// traverse through each column
				//first column is the title
				tr.Children().Each(func(k int, s *goquery.Selection) {

					//fmt.Println("\t\t",k, s.Text())
					switch k {
					case 0:
						item.Name = s.Text()
					case 1:
						item.Price.Regular = s.Text()
					case 2:
						item.Price.Box = s.Text()
					case 3:
						item.Price.SkyBox = s.Text()
					}
				})

			}

			//add to admission item
			adm.Prices = append(adm.Prices, item)
		})
		//add cinema pricing to cinemaAdmission slice
		collection.Items = append(collection.Items, adm)
	})
	return

}
