package main

import (
	"testing"

	"gitlab.com/kingwill101/palaceapi/api"
)

func TestGetCinemas(t *testing.T) {
	api := palaceapi.Api{}

	cin, err := api.GetCinemas()

	if err != nil {
		t.Errorf(err.Error())
	}
	if len(cin) < 0 {
		t.Errorf("didnt find anywhere to go watch a movie :-(")
	}
}

func TestGetPlaying(t *testing.T) {
	api := palaceapi.Api{}
	url := "https://www.palaceamusement.com/cinema.php?id=carib5"

	playing, err := api.GetPlaying(url)

	if err != nil {
		t.Errorf(url, err.Error())
	}

	if len(playing) < 1 {
		t.Errorf(url, "didnt find anything playing :-(")
	}
	t.Log("playing - ", len(playing))

}

func TestComingSoon(t *testing.T) {
	var api palaceapi.Api
	url := "https://www.palaceamusement.com"
	api.SetBaseUrl("https://www.palaceamusement.com")
	comingSoon, err := api.GetComingSoon()

	if err != nil {
		t.Errorf(url, err.Error())
	}

	if len(comingSoon) < 1 {
		t.Errorf("didnt get coming Soon :-(")
	}

	for i := range comingSoon {
		t.Log("coming soon -", comingSoon[i].Title)
	}
}

func TestNowShowing(t *testing.T) {
	var api palaceapi.Api
	url := "https://www.palaceamusement.com"

	api.SetBaseUrl(url)
	nowShowing, err := api.GetNowShowing()

	if err != nil {
		t.Errorf("Something went wrong :-( ", err.Error())
		t.Fail()
	}

	if len(nowShowing) < 1 {
		t.Fail()
	}

	for i := range nowShowing {
		t.Log("showing -", nowShowing[i].Title)
	}
}

func TestGetAdmission(t *testing.T) {
	var api palaceapi.Api

	_, err := api.GetAdmissions()

	if err != nil {
		t.Errorf("Something went wrong :-( ", err.Error())
		t.Fail()
	}

	// if len(admissions) < 1 {
	// 	t.Fail()
	// }

	// for i := range admissions {
	// 	t.Log("showing -", admissions[i].Cinema)
	// }
}
