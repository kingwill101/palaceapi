package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/kingwill101/palaceapi/api"
)

var palaceApi palaceapi.Api

func main() {
	palaceApi.SetBaseUrl("https://www.palaceamusement.com/")
	route()
}

func route() {
	router := gin.Default()

	router.GET("/routes", getRoutes)

	json := router.Group("/api/json")
	json.GET("/cinemas", getCinemas)
	json.GET("/playing/", getPlaying)
	json.GET("/upcoming/", getUpcoming)
	json.GET("/showing/", getShowing)
	json.GET("/admission", admission)

	xml := router.Group("/api/xml")
	xml.GET("/cinemas", getCinemasXML)
	xml.GET("/playing/", getPlayingXML)
	xml.GET("/upcoming/", getUpcomingXML)
	xml.GET("/showing/", getShowingXML)
	xml.GET("/admission", admissionXML)

	router.Run(":" + os.Getenv("PORT"))
}
